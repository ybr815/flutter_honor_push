import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_honor_push/flutter_honor_push.dart';
import 'package:flutter_honor_push/flutter_honor_push_platform_interface.dart';
import 'package:flutter_honor_push/flutter_honor_push_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockFlutterHonorPushPlatform
    with MockPlatformInterfaceMixin
    implements FlutterHonorPushPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');

  @override
  Future<String?> checkSupport() {
    // TODO: implement checkSupport
    throw UnimplementedError();
  }

  @override
  Future<String?> getPushToken() {
    // TODO: implement getPushToken
    throw UnimplementedError();
  }

  @override
  Future<String?> getStatus() {
    // TODO: implement getStatus
    throw UnimplementedError();
  }

  @override
  Future<String?> honorInit() {
    // TODO: implement honorInit
    throw UnimplementedError();
  }

  @override
  Future<String?> turnOff() {
    // TODO: implement turnOff
    throw UnimplementedError();
  }

  @override
  Future<String?> turnOn() {
    // TODO: implement turnOn
    throw UnimplementedError();
  }
}

void main() {
  final FlutterHonorPushPlatform initialPlatform = FlutterHonorPushPlatform.instance;

  test('$MethodChannelFlutterHonorPush is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelFlutterHonorPush>());
  });

  test('getPlatformVersion', () async {
    FlutterHonorPush flutterHonorPushPlugin = FlutterHonorPush();
    MockFlutterHonorPushPlatform fakePlatform = MockFlutterHonorPushPlatform();
    FlutterHonorPushPlatform.instance = fakePlatform;

    expect(await flutterHonorPushPlugin.getPlatformVersion(), '42');
  });
}
