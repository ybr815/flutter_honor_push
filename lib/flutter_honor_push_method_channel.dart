import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'flutter_honor_push_platform_interface.dart';

/// An implementation of [FlutterHonorPushPlatform] that uses method channels.
class MethodChannelFlutterHonorPush extends FlutterHonorPushPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('flutter_honor_push');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<String?> honorInit() async {
    final result = await methodChannel.invokeMethod<String>('honorInit');
    return result;
  }

  @override
  Future<String?> checkSupport() async {
    final result = await methodChannel.invokeMethod<String>('checkSupport');
    return result;
  }

  @override
  Future<String?> getPushToken() async {
    final result = await methodChannel.invokeMethod<String>('getPushToken');
    return result;
  }

  @override
  Future<String?> getStatus() async {
    final result = await methodChannel.invokeMethod<String>('getStatus');
    return result;
  }

  @override
  Future<String?> turnOn() async {
    final result = await methodChannel.invokeMethod<String>('turnOn');
    return result;
  }

  @override
  Future<String?> turnOff() async {
    final result = await methodChannel.invokeMethod<String>('turnOff');
    return result;
  }
}
