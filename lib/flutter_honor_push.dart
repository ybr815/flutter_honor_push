
import 'flutter_honor_push_platform_interface.dart';

class FlutterHonorPush {
  Future<String?> getPlatformVersion() {
    return FlutterHonorPushPlatform.instance.getPlatformVersion();
  }

  Future<String?> honorInit() {
    return FlutterHonorPushPlatform.instance.honorInit();
  }

  Future<String?> checkSupport() {
    return FlutterHonorPushPlatform.instance.checkSupport();
  }

  Future<String?> getPushToken() {
    return FlutterHonorPushPlatform.instance.getPushToken();
  }

  Future<String?> getStatus() {
    return FlutterHonorPushPlatform.instance.getStatus();
  }

  Future<String?> turnOn() {
    return FlutterHonorPushPlatform.instance.turnOn();
  }

  Future<String?> turnOff() {
    return FlutterHonorPushPlatform.instance.turnOff();
  }
}
