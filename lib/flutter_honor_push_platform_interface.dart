import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'flutter_honor_push_method_channel.dart';

abstract class FlutterHonorPushPlatform extends PlatformInterface {
  /// Constructs a FlutterHonorPushPlatform.
  FlutterHonorPushPlatform() : super(token: _token);

  static final Object _token = Object();

  static FlutterHonorPushPlatform _instance = MethodChannelFlutterHonorPush();

  /// The default instance of [FlutterHonorPushPlatform] to use.
  ///
  /// Defaults to [MethodChannelFlutterHonorPush].
  static FlutterHonorPushPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [FlutterHonorPushPlatform] when
  /// they register themselves.
  static set instance(FlutterHonorPushPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<String?> honorInit() {
    throw UnimplementedError('honorInit() has not been implemented.');
  }

  Future<String?> checkSupport() {
    throw UnimplementedError('checkSupport() has not been implemented.');
  }

  Future<String?> getPushToken() {
    throw UnimplementedError('getPushToken() has not been implemented.');
  }

  Future<String?> getStatus() {
    throw UnimplementedError('getStatus() has not been implemented.');
  }

  Future<String?> turnOn() {
    throw UnimplementedError('turnOn() has not been implemented.');
  }

  Future<String?> turnOff() {
    throw UnimplementedError('turnOff() has not been implemented.');
  }
}
