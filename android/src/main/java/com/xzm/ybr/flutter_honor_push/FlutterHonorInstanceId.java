/*
 * Copyright 2020-2024. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xzm.ybr.flutter_honor_push;

import android.content.Context;

import com.hihonor.push.sdk.HonorPushCallback;
import com.hihonor.push.sdk.HonorPushClient;

import io.flutter.plugin.common.MethodChannel.Result;

/**
 * class FlutterHonorInstanceId
 *
 * @since 4.0.4
 */
public class FlutterHonorInstanceId {
//    private static final String TAG = FlutterHonorInstanceId.class.getSimpleName();
//    private final HMSLogger hmsLogger;
    private final Context context;

    public FlutterHonorInstanceId(Context context) {
        this.context = context;
//        hmsLogger = HMSLogger.getInstance(context);
    }

    public void getToken(final Result result) {
        new Thread(() -> {
            HonorPushClient.getInstance().getNotificationCenterStatus(new HonorPushCallback<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    // TODO: 返回应用当前通知栏状态结果
                    result.success("查询状态:" + String.valueOf(aBoolean));
                }

                @Override
                public void onFailure(int errorCode, String errorString) {
                    // TODO: 查询失败
                    result.success(errorString);
                }
            });
        }).start();
    }
}
