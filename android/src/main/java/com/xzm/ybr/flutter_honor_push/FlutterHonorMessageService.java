/*
 * Copyright 2020-2024. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xzm.ybr.flutter_honor_push;

import android.content.Context;
import android.util.Log;

import com.hihonor.push.sdk.HonorMessageService;
import com.hihonor.push.sdk.HonorPushDataMsg;

/**
 * class FlutterHmsMessageService
 *
 * @since 4.0.4
 */
public class FlutterHonorMessageService extends HonorMessageService {
    private static final String TAG = FlutterHonorMessageService.class.getSimpleName();

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        if (PluginContext.getContext() != null) {
//            HMSLogger.getInstance(PluginContext.getContext()).sendPeriodicEvent("onNewToken");
            Log.d(TAG, "Token received");
//            Utils.sendIntent(PluginContext.getContext(), PushIntent.TOKEN_INTENT_ACTION, PushIntent.TOKEN, token);
        }
    }

    @Override
    public void onMessageReceived(HonorPushDataMsg remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Context applicationContext = getApplicationContext();
        boolean isApplicationInForeground = ApplicationUtils.isApplicationInForeground(applicationContext);
        if (isApplicationInForeground) {
//            HMSLogger.getInstance(PluginContext.getContext()).sendPeriodicEvent("onMessageReceived");
            if (remoteMessage != null) {
//                JSONObject jsonObject = new JSONObject(RemoteMessageUtils.toMap(remoteMessage));
//                Utils.sendIntent(PluginContext.getContext(), PushIntent.REMOTE_DATA_MESSAGE_INTENT_ACTION,
//                    PushIntent.DATA_MESSAGE, jsonObject.toString());
            }
        } else {
            PluginContext.initialize(applicationContext);
//            HMSLogger.getInstance(applicationContext).sendPeriodicEvent("onMessageReceived");
//            Intent intent = new Intent(applicationContext, BackgroundMessageBroadcastReceiver.class);
//            intent.setAction(BackgroundMessageBroadcastReceiver.BACKGROUND_REMOTE_MESSAGE);
//            intent.putExtra(Param.MESSAGE.code(), remoteMessage);
//            applicationContext.sendBroadcast(intent);
        }
    }

}
